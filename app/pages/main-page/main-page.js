import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import { BbvaCoreIntlMixin as intl } from '@bbva-web-components/bbva-core-intl-mixin';
import '@bbva-web-components/bbva-header-main/bbva-header-main.js';
import '@bbva-web-components/bbva-foundations-grid-default-layout/bbva-foundations-grid-default-layout.js';
import '@cells-demo/demo-app-template/demo-app-template.js';
import '@bbva-pe-web-components/bbva-core-employee-authentication/bbva-core-employee-authentication.js';
import '@bbva-web-components/bbva-web-template-modal/bbva-web-template-modal.js';
import { securitywarningdark } from '@bbva-web-components/bbva-foundations-microillustrations/bbva-foundations-microillustrations';
import '@bbva-web-components/bbva-web-notification-toast/bbva-web-notification-toast.js';
import '@bbva-web-components/bbva-foundations-spinner/bbva-foundations-spinner.js';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-form-textarea/bbva-web-form-textarea.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select.js';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';
import '@bbva-web-components/bbva-core-generic-dp/bbva-core-generic-dp.js';
import styles from './main-page.css.js';

class MainPage extends intl(CellsPage) {
  static get is() {
    return 'main-page';
  }

  static get properties() {
    return {};
  }

  static get styles() {
    return [ styles ];
  }

  constructor() {
    super();
  }

  firstUpdated() {
    this.runAuth();
  }

  async runAuth() {

  }

  reLogin() {

  }

  openLogin() {

  }

  successAuth({ detail }) {

  }

  idpAssertionError({ detail }) {

  }

  async testService() {

  }

  get _headerTpl() {
    return html`
      <bbva-header-main
        slot="app-header"
        variant="transparent"
        text="App - Autenticación de empleados BBVA"
      ></bbva-header-main>
    `;
  }

  get _utilstemplate() {
    return html`
      <bbva-foundations-spinner class="hide" ></bbva-foundations-spinner>

      <bbva-web-notification-toast variant="success">
        <p>Autenticacion se a efectuado de forma exitosa.</p>
      </bbva-web-notification-toast>

      <bbva-web-template-modal
        help=""
        .helpMicro="${securitywarningdark}"
        heading="Problemas de autenticación"
        button="Volver a intentar"
        @link-click="${this.openLogin}"
        @button-click="${this.reLogin}"
        link="Login"
        linkHref="https://ei-community.grupobbva.com/"
        linkTarget="_blank"
      >
        <p>
          Ha ocurrido un problema al verificar sus credenciales, por favor
          vuelva a intentarlo o valide el login de acceso.
        </p>
      </bbva-web-template-modal>

      <bbva-core-generic-dp id="dp"></bbva-core-generic-dp>
    `;
  }

  /* eslint-disable lit/attribute-value-entities */
  get _mainContentTpl() {
    return html`
      <div class="container-page" slot="app-main-content">
        <main>
          <section class="form">
            <h2>Pruebas de servicios</h2>

            <bbva-web-form-text
              id="host"
              value="https://qa-cotiza-pe.work-02.nextgen.igrupobbva"
              label="Host"
            ></bbva-web-form-text>
            <bbva-web-form-text
              id="path"
              value="customers/v0/customers?identityDocument.documentNumber=41523066&identityDocument.documentType.id=DNI"
              label="Path"
            ></bbva-web-form-text>
            <bbva-web-form-select id="method" label="Método">
              <bbva-web-form-option selected="selected" value="GET"
                >GET</bbva-web-form-option
              >
              <bbva-web-form-option value="POST">POST</bbva-web-form-option>
              <bbva-web-form-option value="PUT">PUT</bbva-web-form-option>
              <bbva-web-form-option value="DELETE">DELETE</bbva-web-form-option>
              <bbva-web-form-option value="PATCH">PATCH</bbva-web-form-option>
            </bbva-web-form-select>
            <bbva-web-form-textarea
              id="body"
              label="Body"
            ></bbva-web-form-textarea>

            <bbva-web-button-default @click="${this.testService}"
              >Ejecutar</bbva-web-button-default
            >
          </section>

          <section class="response">
            <textarea id="text-result" readonly=""></textarea>
          </section>
        </main>

        ${this._utilstemplate}

        <bbva-core-employee-authentication
          id="employeeAuth"
          granting-ticket-url="https://qa-cotiza-pe.work-02.nextgen.igrupobbva/TechArchitecture/pe/grantingTicket/V02"
          aap-id="13000066"
          @employee-authentication-success="${this.successAuth}"
          @idp-assertion-error="${this.idpAssertionError}"
        ></bbva-core-employee-authentication>
      </div>
    `;
  }

  render() {
    return html` <demo-app-template page-title="Main page">
      ${this._headerTpl} ${this._mainContentTpl}
    </demo-app-template>`;
  }
}
window.customElements.define(MainPage.is, MainPage);
